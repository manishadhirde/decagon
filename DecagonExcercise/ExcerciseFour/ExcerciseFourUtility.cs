﻿/*Implement Translator(other language to English).
When below words are entered, their English translation should be printed.
Marathi >> aai, Aag gaadi, paani, poli
Hindi >> maa, rail gaadi, paani, roti
* The code should be extensible. Additional words, languages should not require major code change.
Eg.If user enter aai -> then English translation is “Mother”. 
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace ExcerciseFour
{
    class ExcerciseFourUtility
    {
       static void Main(string[] args)
        {
            Console.WriteLine("Please select the input Language:");
            Console.WriteLine("1.Marathi");
            Console.WriteLine("2.Hindi");
            int Language = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Please enter the word which you want to translate into an English language:");
            string wordNeedToTransalate = Console.ReadLine().ToLower();
            
            List<List<string>> languageCollection = new List<List<string>>();
            List<string> Marathi = new List<string>(new string[] { "aai", "aag gaadi", "paani", "poli" });
            List<string> Hindi = new List<string>(new string[] { "maa", "rail gaadi", "paani", "roti" });
            List<string> English = new List<string>(new string[] { "Mother", "Rail Way", "Water", "Bread" });
            languageCollection.Add(Marathi);
            languageCollection.Add(Hindi);
            languageCollection.Add(English);
            
            if (languageCollection[Language - 1].FindIndex(x => x.Equals(wordNeedToTransalate)) > 0)
            {
                Console.Write("The Translated word is: ");
                Console.Write(languageCollection[2][languageCollection[Language - 1].IndexOf(wordNeedToTransalate)]);
            }
            else
            {
                Console.WriteLine("The word is not found");
            }
            Console.ReadKey();
         }
    }
}
