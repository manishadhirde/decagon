﻿/* Find the greatest divisor of the entered three digit number.*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcerciseSeven
{
    class ExcerciseSevenUtility
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter the Three digit no:");
            int threeDigitNo = Convert.ToInt32(Console.ReadLine());
            List<int> noOfDivisor = new List<int>();
            int divisor = threeDigitNo / 2;

            if (threeDigitNo.ToString().Length.Equals(3))
            {
                if (threeDigitNo%2 == 0)
                {
                    Console.WriteLine("The greatest divisor of the entered no is: " + (threeDigitNo/2));
                }
                else
                {
                    for (int i = 3; i < divisor; i++)
                    {
                        if (threeDigitNo%i == 0)
                        {
                            noOfDivisor.Add(threeDigitNo/i);
                        }

                    }
                    
                    if (noOfDivisor.Count > 0)
                    {
                        Console.WriteLine("The greatest divisor of the entered no is: " +
                                          noOfDivisor.ElementAt(0));
                    }
                    else//eg. 401, 407
                    {
                        Console.WriteLine("It's Prime no:" + threeDigitNo);
                    }
                }
            }
            else
            {
                Console.WriteLine("Data is invalid!! Please enter three Digit no: eg.777 ");
            }
            
            Console.ReadLine();
        }
    }
}
