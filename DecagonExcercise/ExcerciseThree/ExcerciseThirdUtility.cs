﻿/*
 Based on user input, take action on the string-
1. Convert the string to all capitals
2. Convert the string to all small letters
3. Add spaces after each letter
4. Remove spaces
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ExcerciseThree
{
     class ExcerciseThirdUtility
    {
        public static string ConverToUpper(String input)
        {
            return input.ToUpper();
        }

        public static string ConverToLower(String input)
        {
            return input.ToLower();
        }

        public static string AddSpaces(String input)
        {
            string finalString = string.Empty;
            char[] charArray = input.ToCharArray();
            foreach (char item in charArray)
            {
                finalString += item + " ";
            }

            return finalString;
        }

        public static string RemoveAllSpace(String input)
        {
            return input.Replace(" ", null);
        }
    
     static void Main(string[] args)
        {
            Console.WriteLine("--------------------------------------------------------------------");
            Console.WriteLine("Please Enter the string: ");
            string userstring = Console.ReadLine();
            string finalResult = string.Empty;
            Console.WriteLine("Please Choose following action which you want to perform on string :");
            Console.WriteLine("1.Convert the string to all capitals");
            Console.WriteLine("2.Convert the string to all small letters");
            Console.WriteLine("3.Add spaces after each letter");
            Console.WriteLine("4.Remove spaces");
            Console.WriteLine("--------------------------------------------------------------------");
         bool answer = true;
            
         do
         {
             int userInput = Convert.ToInt32(Console.ReadLine());
             switch (userInput)
             {
                 case 1:
                     finalResult = ConverToUpper(userstring);
                     break;

                 case 2:
                     finalResult = ConverToLower(userstring);
                     break;

                 case 3:
                     finalResult = AddSpaces(userstring);
                     break;

                 case 4:
                     finalResult = RemoveAllSpace(userstring);
                     break;

                 default:
                     finalResult = "Your Input is invalid";
                     break;

             }
         

         Console.WriteLine("The final string after action : " + finalResult);
            
                 Console.WriteLine("Do you want to continue[Y/N]");
                 string anwser = Console.ReadLine().ToUpper();
                 if (anwser == "Y")
                 {
                     Console.WriteLine("Please Choose an action which you want to perform on string :");
                 }
                 else
                 {
                      answer = false;
                      Environment.Exit(0);
                 }

            } while (answer);
            
            Console.ReadLine();
         }
    }
}
