﻿/*Shirts are to be stitched from a 6 meters spindle of cloth. 
 User will enter the number of types of shirts required. 
 The cloth will be cut into equal number of parts dedicated to each type of shirt. 
 User will enter the number of shirts and length of cloth required for each shirt-type. 
 Task is to identify the remaining length (if any) of the cloth.

 Input Format-
Line 1: <Types of shirts>
One line for each type: <length of cloth required per shirt> <number of shirts required>
 */
 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ExcerciseTen
{
    internal class ExcerciseTenUtility
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Please enter the no. of Types of shirts:");
            int typeOfShirts = int.Parse(Console.ReadLine());
            double[] addition = new double[typeOfShirts];
            try
            {
                for (int i = 0; i < typeOfShirts; i++)
                {
                    string[] shirt_Temp = Console.ReadLine().Split(' ');
                    double[] shirt = Array.ConvertAll(shirt_Temp, double.Parse);
                    double multiple = shirt[0] * shirt[1];
                    addition[i] += multiple;
                }
                double finalAnswer = addition.Sum();
                finalAnswer = 6 - finalAnswer;
                if (finalAnswer > 0)
                {
                    Console.WriteLine("The remaining length of cloths: " + finalAnswer);
                }
                else
                {
                    Console.WriteLine("The length of cloths is beyond the 6 meters: " + 0);
                }
                
            }
            catch (FormatException ex)
            {
               Console.WriteLine(ex.Message);
                
            }
            Console.ReadLine();
        }
    }
}
