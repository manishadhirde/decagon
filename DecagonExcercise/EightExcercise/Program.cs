﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EightExcercise
{
    class Program
    {
        static void Main(string[] args)
        {
            string inputDate = "09-09-5678";
            DateTime dt;

            bool validDate = DateTime.TryParseExact(inputDate, "dd-MM-yyyy", 
                CultureInfo.InvariantCulture,
                DateTimeStyles.None,
                out dt);

            Console.WriteLine(validDate ? dt.ToString("dd-MM-yyyy") : "Invalid date");

        }
    }
}
