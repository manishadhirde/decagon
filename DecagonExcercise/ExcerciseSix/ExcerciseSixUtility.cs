﻿/*An online portal sells 5 items (Items will be named iOne,iTwo,iThree,iFour,iFive).
Input will be a coma separated string indicating the list of items bought (ex. iTwo,iThree,iOne,iTwo)
Output will be the item which has been bought most.
* The code should be extensible. Additional items should not require major code change.
 */
 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcerciseSix
{
    class ExcerciseSixUtility
    {
        private static void Main(string[] args)
        {
            //string[] items = {"iOne", "iTwo", "iThree", "iFour", "iFive"};
            List<string> itemsList = new List<string>();
            itemsList.Add("iOne");
            itemsList.Add("iTwo");
            itemsList.Add("iThree");
            itemsList.Add("iFour");
            itemsList.Add("iFive");

            Console.WriteLine("Please enter the list of items bought: ");
            string[] listOfItems = Console.ReadLine().Split(',');

            if (listOfItems.Intersect(itemsList).Count() > 0)
            {
                var groups = listOfItems.GroupBy(v => v);
                var maxRepeatedItem = listOfItems.GroupBy(x => x)
                    .OrderByDescending(x => x.Count())
                    .First().Count();

                foreach (var group in groups)
                {
                    if (group.Count() == maxRepeatedItem)
                    {
                        Console.WriteLine("Item which has been bought most is: " + group.Key);
                    }
                }

            }


            //if (listOfItems.Intersect(itemsList).Count() > 0)
            //{
            //    //var maxRepeatedItem = listOfItems.GroupBy(x => x)
            //    //    .OrderByDescending(x => x.Count())
            //    //   .First().Key;
            //   Console.WriteLine("Item which has been bought most is: " + maxRepeatedItem);
            //}

            else
            {
                Console.WriteLine("Please enter the input in valid formmate");
            }

            Console.ReadLine();
                
            }
    }
}
