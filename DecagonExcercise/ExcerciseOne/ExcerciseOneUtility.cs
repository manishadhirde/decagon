﻿/* A. User will enter a two digit number.(57)
B. Add the digits of the number to a sum.(12)
C. Subtract the sum from the two digit.(45)
D. Print the sum of the digits obtained as a result of 'C'.(9)
*/

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Configuration;
using System.Text;
using System.Threading.Tasks;

namespace ExcerciseOne
{
    class ExcerciseOneUtility
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter two digit no:");
            string inputValue = Console.ReadLine();
            
            try
            {
                int input = inputValue.Count();
                int integerValue = Int32.Parse(inputValue);
                if (input.Equals(2))
                {
                    int sum = Math.Abs(integerValue / 10) + integerValue % 10;
                    int subtract = integerValue - sum;
                    int finalAddition = Math.Abs(subtract / 10) + subtract % 10;
                    Console.WriteLine("The final no is:" + finalAddition);
                }
                else
                {
                    Console.WriteLine("Your input is invalid! Please enter two digit no");
                }

            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.Message);
            }
            
            Console.ReadLine();
        }
    }
}
