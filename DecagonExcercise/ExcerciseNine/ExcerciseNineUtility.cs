﻿/*People have gathered to attend a training. Number of people will be entered by the user.
Groups of people are to be formed. The group size will be entered by user.
People in the group will be given tokens. Cost of the token will be entered by user.
The remaining people who cannot be grouped, will be given a gift each. Cost of the gift will be entered by user.
OUtput the total cost for the training as result of the above.
Input format: <Number of people> <Size of group> <Cost of token> <Cost of gift>
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Text;
using System.Threading.Tasks;

namespace ExcerciseNine
{
    class ExcerciseNineUtility
    {
        static void Main(string[] args)
        {
            try
            {

                int finalCost;
                Console.WriteLine("Please enter Number of people: ");
                int people = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Please enter group size: ");
                int groupSize = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Please enter Cost of the token: ");
                int costOfToken = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Please enter Cost of the gift: ");
                int costOfGift = Convert.ToInt32(Console.ReadLine());

                int remainingPople = people%groupSize;
                int totalCostOfToken = people*costOfToken;
                if (remainingPople >= 1)
                {
                    int totalGiftCost = remainingPople*costOfGift;
                    finalCost = totalGiftCost + totalCostOfToken;
                    Console.WriteLine("Final Cost for training is: " + finalCost);
                }
                else
                {
                    Console.WriteLine("Final Cost for training is: " + totalCostOfToken);
                }
            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.ReadLine();

        }
    }
}
