﻿/*Input two strings. Perform the below operations, and print the output.
String 1 -> Replace 'A' with ',' and 'B' with ';'
String 2 -> REplace 'Q' with '_' and 'R' with '#'
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcerciseEight
{
    internal class ExcerciseEightUtility
    {
        public static void Main(string[] args)
        {
            string firstString = Console.ReadLine().ToUpper();
            string secondString = Console.ReadLine().ToUpper();
            string checkingElements = "ABQR";
            string replacingElements = ",;_#";

            for (int i = 0; i < checkingElements.Length-1; i++)
            {
                if (firstString.Contains(checkingElements[i]) || firstString.Contains(checkingElements[i+1]))
                {
                    if (firstString.Contains(checkingElements[i]))
                    {
                        firstString = firstString.Replace(checkingElements[i], replacingElements[i]);
                    }
                    else
                    {
                        firstString = firstString.Replace(checkingElements[i+1], replacingElements[i+1]);
                    }
                }
                else
                {
                    if (secondString.Contains(checkingElements[i+2]))
                    {
                        secondString = secondString.Replace(checkingElements[i+2], replacingElements[i+2]);
                    }
                    else
                    {
                        secondString = secondString.Replace(checkingElements[i+3], replacingElements[i+3]);
                    }
                }
            }

            Console.WriteLine("After performing the operation first string is :" + firstString);
            Console.WriteLine("After performing the operation second string is :" + secondString);
            Console.ReadLine();
        }
    }
}
