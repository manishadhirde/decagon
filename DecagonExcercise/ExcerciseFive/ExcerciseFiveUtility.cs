﻿/*Implement date validator for Format DD-MM-YYYY.*/

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcerciseFive
{
    class ExcerciseFiveUtility
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter the date: ");
            string date = Console.ReadLine();
            DateTime dob;
            bool validDate = DateTime.TryParseExact(date, "dd-MM-yyyy",
                CultureInfo.InvariantCulture,
                DateTimeStyles.None,
                out dob);
            
            if (validDate)
            {
                Console.WriteLine("Date Format is valid:" + date);
            }
            else
            {
                Console.WriteLine("Date Format is not valid:" + date);
            }
            Console.ReadLine();
        }
    }
}
