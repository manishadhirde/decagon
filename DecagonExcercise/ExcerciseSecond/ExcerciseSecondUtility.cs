﻿/*Tokenize the input string using vowels as delimiters.
Sample Input String-
CRICKETISPLAYEDWITHELEVENPLAYERS
OUTPUT-
CR,CK,T,SPL,Y,DW,TH,L,V,NPL,Y,RS*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcerciseSecond
{
    class ExcerciseSecondUtility
    {
        static void Main(string[] args)
        {
            string vowels = "aeoui";
            Console.WriteLine("Please enter the input string:");
            string inputString = Console.ReadLine().ToLower();
            foreach (char vowel in vowels)
            {
                inputString = inputString.Replace(vowel, ',');
            }

            Console.WriteLine("The Final string is: " + inputString);
            Console.ReadLine();
            
        }
    }
}
